# storage/ssd/nvme_fw_update_reflect_new_fw_version

Storage: test nvme list output reflect new fw version

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
#### You need run test_dev_setup to define TEST_DEVS
```bash
bash ./runtest.sh
```
