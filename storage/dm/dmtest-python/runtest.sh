#!/bin/bash
#
# Copyright (c) 2020-2021 Red Hat, Inc. All rights reserved.
#
# This copyrighted material is made available to anyone wishing
# to use, modify, copy, or redistribute it subject to the terms
# and conditions of the GNU General Public License version 2.
#
# This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#
# shellcheck source=/dev/null
source ../include/libdmtest.sh
export PATH="$PATH":~/.cargo/bin

RESULT_SET="cki_dmtest"
TEST_FILTERS_LINUX_REPO_UNAVAILABLE="--and-filters \
--rx=^/(?!thin/snapshot/(many-snaps-with-changes|try-and-create-duplicates|parallel-io-to-shared-thins)) \
--rx=^/(?!blk-archive/rolling-snaps)' --rx=^/(?!thin/fs-bench) --rx=^/(?!thin/discard)"
TEST_FILTERS_LINUX_REPO="--and-filters \
--rx=^/(?!thin/snapshot/parallel-io-to-shared-thins) --rx=^/(?!thin/fs-bench/) --rx=^/(?!thin/discard)"

startup
pushd "$DMTS_LOCAL" || return 1
./dmtest health
echo "Cloning linux repo"
if ! clone_linux_repo; then
  echo "Skipping tests that require linux repo."
  runtest "$RESULT_SET" "$TEST_FILTERS_LINUX_REPO_UNAVAILABLE"
else
  runtest "$RESULT_SET" "$TEST_FILTERS_LINUX_REPO"
fi
report_results $RESULT_SET
