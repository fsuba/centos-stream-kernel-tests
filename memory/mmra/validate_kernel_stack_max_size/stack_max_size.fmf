summary: Verifies that max kernel stack size is less than 16k
description: |
    Test for stack_max_size. And this should be ran at the end of
    a test round but without the machine being rebooted, because
    we want to find offenders in drivers.

    The test works as follows:
    	Verifies that max kernel stack size is less than 16k

    First it checks if the stack tracer is enabled and exits if not.

    If enabled it checks the current stack_max_size value, and compares it against a predefined threshold.

    If the stack_max_size exceeds the threshold, a warning is logged, and the
    function responsible for the maximum stack usage is identified.

    Inputs:
        /sys/kernel/tracing/stack_max_size
        /sys/kernel/tracing/stack_trace
        /proc/sys/kernel/stack_tracer_enabled

    Expected resluts:
        [   INFO   ] :: stack_max_size (<size> bytes) is within the acceptable range.

    Results location:
        output.txt.
contact: mm-qe <mm-qe@redhat.com>
test: bash ./runtest.sh
framework: beakerlib
id: 519bce43-f03c-4345-bad4-b8ec9382257a
duration: 15m
extra-summary: validate_kernel_stack_max_size
extra-task: validate_kernel_stack_max_size
